using Toybox.WatchUi;
using Toybox.Application.Properties;

import Toybox.Lang;

class SettingsMenu extends WatchUi.Menu2 {

    function initialize() {
        Menu2.initialize(null);
        Menu2.setTitle("Settings");
        Menu2.addItem(new WatchUi.MenuItem("Min SPO2", null, "minO2", null));
        Menu2.addItem(new WatchUi.MenuItem("Min HR", null, "minHR", null));
        Menu2.addItem(new WatchUi.MenuItem("Vibrate", null, "vibrate", null));
        Menu2.addItem(new WatchUi.MenuItem("Sensor Poll", null, "sensorPoll", null));
        Menu2.addItem(new WatchUi.MenuItem("Display Update", null, "displayUpdate", null));
    }
}

class SettingsMenuDelegate extends WatchUi.Menu2InputDelegate {

    function initialize() {
        Menu2InputDelegate.initialize();
    }

    function onSelect(menuItem) {
        if (menuItem.getId().equals("minO2")) {
            var minSPO2 = Properties.getValue("minSPO2") as Numeric;
            var spo2Picker = new NumberPicker("Min SPO2", new NumberFactory(50, 100, 1, null), minSPO2);

            WatchUi.pushView(
                spo2Picker,
                new MinO2PickerDelegate(spo2Picker),
                WatchUi.SLIDE_IMMEDIATE
            );
        } else if (menuItem.getId().equals("minHR")) {
            var minHR = Properties.getValue("minHR") as Numeric;

            var hrPicker = new NumberPicker("Min HR", new NumberFactory(30, 120, 1, null), minHR);

            WatchUi.pushView(
                hrPicker,
                new MinHRPickerDelegate(hrPicker),
                WatchUi.SLIDE_IMMEDIATE
            );
        } else if (menuItem.getId().equals("vibrate")) {
            var vibrate = Properties.getValue("vibrate") as Numeric;

            var vibratePicker = new NumberPicker("Vibrate %", new NumberFactory(0,100,25, null), vibrate);

            WatchUi.pushView(
                vibratePicker,
                new VibratePickerDelegate(vibratePicker),
                WatchUi.SLIDE_IMMEDIATE
            );
        } else if (menuItem.getId().equals("sensorPoll")) {
            var seconds = Properties.getValue("sensorPollSeconds") as Numeric;
            var index = 1;
            if(seconds < 4000) {
                index = 0;
            } else if (seconds > 4000) {
                index = 2;
            }

            var frequencyPicker = new FrequencyPicker("Frequency", index);

            WatchUi.pushView(
                frequencyPicker,
                new SensorPollPickerDelegate(frequencyPicker),
                WatchUi.SLIDE_IMMEDIATE
            );
        } else if (menuItem.getId().equals("displayUpdate")) {
            var seconds = Properties.getValue("uiRefreshSeconds") as Numeric;
            var index = 1;
            if(seconds < 10000) {
                index = 0;
            } else if (seconds > 10000) {
                index = 2;
            }

            var frequencyPicker = new FrequencyPicker("Frequency", index);

            WatchUi.pushView(
                frequencyPicker,
                new DisplayUpdatePickerDelegate(frequencyPicker),
                WatchUi.SLIDE_IMMEDIATE
            );
        }
    }
}