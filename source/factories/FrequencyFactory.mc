using Toybox.Graphics;
using Toybox.WatchUi;
import Toybox.Lang;

class FrequencyFactory extends WatchUi.PickerFactory {

    function getDrawable(index, selected) {
        var curValue = getValue(index) as String;
        return new WatchUi.Text( { :text=>curValue, :color=>Graphics.COLOR_WHITE, :font=> Graphics.FONT_MEDIUM, :locX =>WatchUi.LAYOUT_HALIGN_CENTER, :locY=>WatchUi.LAYOUT_VALIGN_CENTER } );
    }

    function initialize() {
        PickerFactory.initialize();
    }

    function getValue(index) {
        var value = "";
        switch(index) {
            case 0:
            value = "Short";
            break;
            case 1:
            value = "Medium";
            break;
            case 2:
            value = "Long";
            break;
        }

        return value;
    }

    function getSize() {
        return 3;
    }
}