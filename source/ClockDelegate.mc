import Toybox.Lang;
import Toybox.WatchUi;

class ClockDelegate extends WatchUi.BehaviorDelegate {
    function initialize() {
        BehaviorDelegate.initialize();
    }

    function onNextPage() {
        WatchUi.pushView(
            new StatsView(),
            new StatsDelegate(),
            WatchUi.SLIDE_UP
        );

        return true;
    }

    function onBack() {
        var dialog = new WatchUi.Confirmation("Exit O2Sleep?");
        WatchUi.pushView(
            dialog,
            new ConfirmExitDelegate(),
            WatchUi.SLIDE_IMMEDIATE
        );

        return true;
    }

    (:appsettings)
    function onMenu() {
        WatchUi.pushView(
            new SettingsMenu(),
            new SettingsMenuDelegate(),
            WatchUi.SLIDE_IMMEDIATE
        );

        return true;
    }
}

class ConfirmExitDelegate extends WatchUi.ConfirmationDelegate {
    function initialize() {
        ConfirmationDelegate.initialize();
    }

    function onResponse(response) {
        if (response == WatchUi.CONFIRM_YES) {
            System.exit();
        }
        return true;
    }
}