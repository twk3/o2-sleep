using Toybox.Application;
using Toybox.Graphics;
using Toybox.WatchUi;
using Toybox.Application.Properties;

import Toybox.Lang;

class FrequencyPicker extends WatchUi.Picker {
    hidden var mTitleText;
    hidden var mFactory;

     function initialize(mTitleText as String, initial as Numeric) {
        mTitle = new WatchUi.Text({:text=>mTitleText, :locX =>WatchUi.LAYOUT_HALIGN_CENTER, :locY=>WatchUi.LAYOUT_VALIGN_BOTTOM, :color=>Graphics.COLOR_WHITE});
        Picker.initialize({:title=>mTitle, :pattern=>[new FrequencyFactory()], :defaults=>[initial]});
     }
}

class SensorPollPickerDelegate extends CustomPickerDelegate {
    function initialize(picker) {
        CustomPickerDelegate.initialize(picker);
    }

    function onAccept(values) {
        var value = 4000;
        switch(values[0]) {
            case "Short":
            value = 1000;
            break;
            case "Medium":
            value = 4000;
            break;
            case "Long":
            value = 10000;
            break;
        }

        Properties.setValue("sensorPollSeconds", value);
        Application.getApp().monitor.readSettings();
        CustomPickerDelegate.onAccept(values);

        return true;
    }
}

class DisplayUpdatePickerDelegate extends CustomPickerDelegate {
    function initialize(picker) {
        CustomPickerDelegate.initialize(picker);
    }

    function onAccept(values) {
        var value = 10000;
        switch(values[0]) {
            case "Short":
            value = 5000;
            break;
            case "Medium":
            value = 10000;
            break;
            case "Long":
            value = 30000;
            break;
        }

        Properties.setValue("uiRefreshSeconds", value);
        Application.getApp().controller.readSettings();
        CustomPickerDelegate.onAccept(values);

        return true;
    }
}