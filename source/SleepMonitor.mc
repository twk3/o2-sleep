using Toybox.Sensor;
using Toybox.Timer;
import Toybox.Application.Properties;
import Toybox.Lang;

typedef CallbackContainer as Dictionary<String, Method(a as Number)>;

(:background)
class SleepMonitor {

    hidden var mTimer;
    hidden var mCurrentO2;
    hidden var mCurrentHR;
    hidden var alertListeners as Dictionary<String, CallbackContainer>;
    hidden var alerting as Boolean;
    hidden var minO2 as Numeric;
    hidden var minHR as Numeric;
    hidden var alertBackoffSeconds as Numeric;
    hidden var sensorPollSeconds as Numeric;
    hidden var lastAlertTime as Numeric;

    function initialize() {
        alertListeners = { "hr" => {}, "o2" => {} };
        mCurrentO2 = 0;
        mCurrentHR = 0;
        alerting = false;
        alertBackoffSeconds = Properties.getValue("alertBackoffSeconds");
        sensorPollSeconds = Properties.getValue("sensorPollSeconds");
        minO2 = Properties.getValue("minSPO2");
        minHR = Properties.getValue("minHR");
        lastAlertTime = 0;
    }

    function addAlertListener(type as String, key as String, handler as Method(a as Number)) {
        alertListeners[type][key] = handler;
    }

    function removeAlertListener(type as String, key as String) {
        if (alertListeners[type].hasKey(key)) {
            alertListeners[type].remove(key);
        }
    }

    function runAlerts(type as String, value as Numeric) {
        var alertsToRun = alertListeners[type].values();
        for( var i = 0; i < alertsToRun.size(); i++ ) {
            alertsToRun[i].invoke(value);
        }
    }

    function readSettings() {
        minO2 = Properties.getValue("minSPO2");
        minHR = Properties.getValue("minHR");
    }

    function start() {
        Sensor.enableSensorType(Sensor.SENSOR_ONBOARD_HEARTRATE);
        Sensor.enableSensorType(Sensor.SENSOR_ONBOARD_PULSE_OXIMETRY);
        onTimer();
        mTimer = new Timer.Timer();
        mTimer.start(method(:onTimer), sensorPollSeconds, true);
    }

    function stop() {
        if (mTimer != null) {
            mTimer.stop();
        }
    }

    function getCurrentHR() {
        return mCurrentHR;
    }

    function getCurrentO2() {
        return mCurrentO2;
    }

    function onTimer() as Void {
        var info = Sensor.getInfo();
        mCurrentHR = info.heartRate;
        mCurrentO2 = info.oxygenSaturation;

        var timeNow = Time.now().value();

       // O2 Alerting
       if (minO2 != null && mCurrentO2 != null && mCurrentO2 > 0) {
            if (mCurrentO2 < minO2 && (lastAlertTime == 0 || timeNow > (lastAlertTime + alertBackoffSeconds))) {
                lastAlertTime = timeNow;
                runAlerts("o2", mCurrentO2);

                return;
            }
        }

       // HR Alerting
       if (minHR != null && mCurrentHR != null && mCurrentHR > 0) {
            if (mCurrentHR < minHR && (lastAlertTime == 0 || timeNow > (lastAlertTime + alertBackoffSeconds))) {
                lastAlertTime = timeNow;
                runAlerts("hr", mCurrentHR);

                return;
            }
        }
    }
}