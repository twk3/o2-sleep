import Toybox.System;

(:background)
class BackgroundService extends System.ServiceDelegate {

    function initialize() {
        ServiceDelegate.initialize();
    }

    public function onTemporalEvent() as Void {
        Application.getApp().monitor.addAlertListener("hr", "background", method(:launchHRAlert));
        Application.getApp().monitor.addAlertListener("o2", "background", method(:launchO2Alert));
        Application.getApp().monitor.onTimer();
        Background.exit(null);
    }

    function launchO2Alert(current) as Void {
        Background.requestApplicationWake("Low O2: " + current.format("%d"));
    }

    function launchHRAlert(current) as Void {
        Background.requestApplicationWake("Low HR: " + current.format("%d"));
    }

    public function onSleepTime() as Void {
        Background.requestApplicationWake("Almost bedtime, launch 02Sleep?");
        Background.exit(null);
    }
}